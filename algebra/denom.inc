#procedure NumDen

 S x1,x2;
 S qq1,...,qq15;

 Factarg,Den;
 id Den(x1?,x2?,?a)=Den(x1)*Den(x2)*Den(?a);
 id Den(-1)=-1;
 id Den(1)=1;
 id Den(-1/2)=-2;
 id Den(1/2)=2;
 id Den(1/4)=4;
 id Den(-1/4)=-4;
 id Den=1;

 id Den(-1 + d) = qq1;
 id Den(-2 + d) = qq2;
 id Den(-3 + d) = qq3;
 id Den(-4 + d) = qq4;
 id Den(t1) = qq5;
 id Den(sll) = qq6;
 id Den(t2) = qq7;
 id Den(m)^2 = qq8;
 id Den(tll) = qq9;
 id Den(4*m^2 - sll) = qq10;
 id Den(-m^2 + tll) = qq11;
 id Den(m^2 - sll + t1 + t2 - tll) = qq12;
 id Den(sll^2 - 2*sll*t1 + t1^2 - 2*sll*t2 - 2*t1*t2 + t2^2) =  qq13;
 id Den(m^4 - 2*m^2*t1 + t1^2 - 2*m^2*tll - 2*t1*tll + tll^2) =  qq14;
 id Den(m^4 - 2*m^2*t2 + t2^2 - 2*m^2*tll - 2*t2*tll + tll^2) =  qq15;
 id Den(m^4*sll - m^2*sll*t1 + m^2*t1^2 - m^2*sll*t2 - 2*m^2*t1*t2 +  sll*t1*t2 + m^2*t2^2 - 2*m^2*sll*tll + sll^2*tll - sll*t1*tll -  sll*t2*tll + sll*tll^2) = qq16;
 id Den(1 - d) = -qq1;
 id Den(2 - d) = -qq2;
 id Den(3 - d) = -qq3;
 id Den(4 - d) = -qq4;
 id Den(-t1) = -qq5;
 id Den(-sll) = -qq6;
 id Den(-t2) = -qq7;
 id Den(-m)^2 = -qq8;
 id Den(-tll) = -qq9;
 id Den(-4*m^2 + sll) = -qq10;
 id Den(m^2 - tll) = -qq11;
 id Den(-m^2 + sll - t1 - t2 + tll) = -qq12;
 id Den(-sll^2 + 2*sll*t1 - t1^2 + 2*sll*t2 + 2*t1*t2 - t2^2) = -qq13;
 id Den(-m^4 + 2*m^2*t1 - t1^2 + 2*m^2*tll + 2*t1*tll - tll^2) = -qq14;
 id Den(-m^4 + 2*m^2*t2 - t2^2 + 2*m^2*tll + 2*t2*tll - tll^2) = -qq15;
 id Den(-(m^4*sll) + m^2*sll*t1 - m^2*t1^2 + m^2*sll*t2 + 2*m^2*t1*t2 - sll*t1*t2 - m^2*t2^2 + 2*m^2*sll*tll - sll^2*tll + sll*t1*tll + sll*t2*tll - sll*tll^2) = -qq16;

#endprocedure


