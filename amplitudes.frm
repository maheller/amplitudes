
*#:workspace 100M
#:TermsInSmall 500000
#: SmallSize 5000M
#:largesize 5000M
#:smallextension 6000M
#:scratchsize 4000M

V p1,p2,p3,p4;
CF Dia,SignFac,SymmFac,NumLegs,NumPropagators,Leg,Pos,Mom,Vertex,Propagator,Mass;
NT epsTimesg5;
S photon,L,LBar;

#include procedures/Fermiketten
#include procedures/PropagatorRules
#include procedures/Vertexrules

#include algebra/denom.inc

#include process/kinematic.inc


#procedure Simplify
***mu1 to the left***
repeat id g0(1,?a,mu2?!{mu1},mu1,?b)=2*d_(mu1,mu2)*g0(1,?a,?b)-g0(1,?a,mu1,mu2,?b);
***mu3 next to mu1***
repeat id g0(1,?a,mu2?!{mu1,mu3},mu3,?b)=2*d_(mu3,mu2)*g0(1,?a,?b)-g0(1,?a,mu3,mu2,?b);

**Dirac EQ on v
repeat;
id g0(1,?a,p4)*v(p4)=-m*g0(1,?a)*v(p4);
id g0(1,?a,p4,mu1?!{p4},?b)=2*p4(mu1)*g0(1,?a,?b)-g0(1,?a,mu1,p4,?b);
endrepeat;

**Dirac EQ on u
repeat;
id u(p3)*g0(1,p3,?a)=m*u(p3)*g0(1,?a);
id g0(1,?a,mu1?!{p3},p3,?b)=2*p3(mu1)*g0(1,?a,?b)-g0(1,?a,p3,mu1,?b);
endrepeat;

*id g0(1,mu1,mu3,p?)=d_(mu1,mu3)*g0(1,p)+1/2*sigma(mu1,mu3)*g0(1,p1);
id g0(1,mu1,mu3,p?)=d_(mu1,mu3)*g0(1,p)+p(mu3)*g0(1,mu1)-p(mu1)*g0(1,mu3)+epsTimesg5(1,mu1,mu3,p);
id g0(1)=1;

*id p1(mu1)=0;
*id p2(mu3)=0;
*id p1(mu3)=p3(mu3)+p4(mu3);
*id p2(mu1)=p3(mu1)+p4(mu1);

id g0(1,?a,mu3?!{p},p?,p?,?b)=g0(1,?a,mu3,?b)*p.p;
#endprocedure

S qq0,qq1,...,qq16;
S m2;
V k1
CF SKP;
NT sigma;
I mu,nu;

Format Mathematica;
Dimension d;
S diagrams;
Local M=diagrams;

#include dileptontensor.inc

**FURRY***
id Dia(7)=0;
id Dia(8)=0;
***********



.sort
#call Fermiketten
.sort
#call PropagatorRules
.sort
#call kinematic
.sort
#call Vertexrules
.sort
#call NumDen




id Leg(photon,?a)=1;
id SignFac(x?)=x;
id SymmFac(x?)=x;
id NumPropagators(?a)=1;
id NumLegs(?a)=1;

#call Simplify
*multiply p1(mu1);
#call Simplify
.sort
#call kinematic
#call Simplify

.sort
#call kinematic
repeat id m^2=m2;

id Dia(?a)=1;
id p1(mu3?)=-p2(mu3)+p3(mu3)+p4(mu3);

bracket sigma,d_,g0,v,e_,p3,p4,x,p1,p2,QL,e;
print;

.end
