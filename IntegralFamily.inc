#procedure INTProp

id Sector(Ax34,?a)=Sector(A,?a)*Crossed(3,4);
id Sector(A,?a)=Sector(A,?a)*INT(A);
id INT(x1?)=INT(x1,[],0,0,0,0);

* Intgeralfamilie A
*
*      - [k1,       m^2]
*      - [k1-p1,    m^2]
*      - [k1-p3-p4, m^2]
*      - [k1-p3,      0]

repeat;
id Prop(k1, m) *INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1+1,x2,x3,x4);
id Prop(-k1, m)*INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1+1,x2,x3,x4);

id Prop(k1-p1, m) *INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2+1,x3,x4);
id Prop(-k1+p1, m)*INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2+1,x3,x4);

id Prop(k1-p1-p2, m) *INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2,x3+1,x4);
id Prop(-k1+p1+p2, m)*INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2,x3+1,x4);

id Prop(k1-p3, 0) *INT(x6?,[],x1?,x2?,x3?,x4?)=  INT(x6,[],x1,x2,x3,x4+1);
id Prop(-k1+p3, 0)*INT(x6?,[],x1?,x2?,x3?,x4?)=  INT(x6,[],x1,x2,x3,x4+1);

endrepeat;
#endprocedure

#procedure INTInvProp
repeat;
id InvProp(k1, m) *INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1-1,x2,x3,x4);
id InvProp(-k1, m)*INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1-1,x2,x3,x4);

id InvProp(k1-p1, m) *INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2-1,x3,x4);
id InvProp(-k1+p1, m)*INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2-1,x3,x4);

id InvProp(k1-p1-p2, m) *INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2,x3-1,x4);
id InvProp(-k1+p1+p2, m)*INT(x6?,[],x1?,x2?,x3?,x4?)=INT(x6,[],x1,x2,x3-1,x4);

id InvProp(k1-p3, 0) *INT(x6?,[],x1?,x2?,x3?,x4?)=  INT(x6,[],x1,x2,x3,x4-1);
id InvProp(-k1+p3, 0)*INT(x6?,[],x1?,x2?,x3?,x4?)=  INT(x6,[],x1,x2,x3,x4-1);

endrepeat;

id Prop(p?,0)=Den(p.p);
id Prop(p?,m1?)=Den(p.p-m1^2);
#endprocedure
