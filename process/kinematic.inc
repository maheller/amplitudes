#procedure kinematic

 C Den;
 S m,t1,t2,sll,tll;
 V p1,p2,p3,p4;

 id p1.p1 = t1;
 id p2.p2 = t2;
 id p3.p3 = m^2;
 id p4.p4 = m^2;
 multiply replace_(p4,p1+p2-p3);
 .sort
 id p1.p3=1/2*m^2-1/2*tll+1/2*t1;
 id p2.p3=-1/2*m^2+1/2*sll+1/2*tll-1/2*t1;
 id p2.p1=1/2*sll-1/2*t2-1/2*t1;

 bracket Den;
 .sort
 keep brackets;
 argument;
 id p1.p1 = t1;
 id p2.p2 = t2;
 id p3.p3 = m^2;
 id p4.p4 = m^2;
 endargument;
 bracket Den;
 .sort
 argument;
 id p1.p3=1/2*m^2-1/2*tll+1/2*t1;
 id p2.p3=-1/2*m^2+1/2*sll+1/2*tll-1/2*t1;
 id p2.p1=1/2*sll-1/2*t2-1/2*t1;
 endargument;

 multiply replace_(p2,p3+p4-p1);
 id p1.p1 = t1;
 id p2.p2 = t2;
 id p3.p3 = m^2;
 id p4.p4 = m^2;


#endprocedure

